import Foundation

class HomePresenter: HomeModuleInput, HomeViewOutput, HomeInteractorOutput {
    
    weak var view: HomeViewInput!
    var interactor: HomeInteractorInput!
    var router: HomeRouterInput!
    
    // MARK: - HomeViewOutput
    func configureModule() {
        
    }
    
    func navigatePranaScreen(){
        router.navigatePranaScreen()
    }
    
    func navigateDhyanaScreen() {
        router.navigateDhyanaScreen()
    }
    
    func navigateMindfulMovementScreen() {
        router.navigateMindfulMovementScreen()
    }
    
    func navigateMindfulEatingScreen() {
        router.navigateMindfulEatingScreen()
    }
    // MARK: - HomeInteractorOutput
    
}
