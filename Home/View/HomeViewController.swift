import UIKit

/**
 This class basically implemented for main Home screen.
 1) It handles button actions of disclaimer,aaswada,dhyana,dhwani,prana screens.
 */
class HomeViewController: UIViewController, HomeViewInput,DisclaimerViewDelegate {
    
    var output: HomeViewOutput!
    ///This property holds network connectivity status.
    let network: ConnectivityManager = ConnectivityManager.shared
    ///This property holds DisclaimerView
    var disclaimer = DisclaimerView.initView()
    ///This property holds logoImage Top constraint  .
    @IBOutlet weak var logoImageTopMargin: NSLayoutConstraint!
    ///This property holds Welcome Top constraint  .
    @IBOutlet weak var welcomeLabelTopMargin: NSLayoutConstraint!
    ///This property holds Info label object.
    @IBOutlet weak var infoLabel: UILabel!
    ///This property holds stackview bottom constraint  .
    @IBOutlet weak var ButtonStackViewBottomMargin: NSLayoutConstraint!
    ///This property holds welcome label object  .
    @IBOutlet weak var welcomeLabel: UILabel!
    ///This property holds aaswada button width constraint  .
    @IBOutlet weak var aaswadaButtonWidth: NSLayoutConstraint!
    ///This property holds aaswada button height constraint  .
    @IBOutlet weak var aaswadaButtonHeight: NSLayoutConstraint!
    ///This property holds dwani Button Height constraint  .
    @IBOutlet weak var dwaniButtonHeight: NSLayoutConstraint!
    ///This property holds dwani Button Width constraint  .
    @IBOutlet weak var dwaniButtonWidth: NSLayoutConstraint!
    ///This property holds dhana Button Width constraint  .
    @IBOutlet weak var dhanaButtonWidth: NSLayoutConstraint!
    ///This property holds dhana Button Width constraint  .
    @IBOutlet weak var dhanaButtonHeight: NSLayoutConstraint!
    ///This property holds prana Button Heigh constraint  .
    @IBOutlet weak var pranaButtonHeight: NSLayoutConstraint!
    ///This property holds prana Button Width constraint  .
    @IBOutlet weak var pranaButtonWidth: NSLayoutConstraint!
    ///This property holds app Icon Height constraint  .
    @IBOutlet weak var appIconHeight: NSLayoutConstraint!
    ///This property holds app Icon Width constraint  .
    @IBOutlet weak var appIconWidth: NSLayoutConstraint!
    ///This property holds setting Button height constraint  .
    @IBOutlet weak var settingButtonheight: NSLayoutConstraint!
    ///This property holds setting Button Width constraint  .
    @IBOutlet weak var settingButtonWidth: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addSubView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkInternetConnetionStatus()
    }
    
    
    
    /** This function get the status of rechability.
- ## Logical SetUp :
     1) ConnectivityManager.isReachable / network.reachability.whenReachable will indicates their is internet connection in device.
     2) ConnectivityManager.isUnreachable / network.reachability.whenUnreachable will indicates their is no internet connection in device.
     */
    func checkInternetConnetionStatus() {
        ConnectivityManager.isReachable { networkManagerInstance in
            
        }
        ConnectivityManager.isUnreachable { networkManagerInstance in
            UIAlertController().internetsetting(vc: self)
        }
        
        network.reachability.whenReachable = { _ in
            DispatchQueue.main.async {
            }
        }
        
        network.reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                UIAlertController().internetsetting(vc: self)
            }
        }
    }
    
    /** This Button action will trigger the function that will help to navigate to **Aaswada Screen**. */
    @IBAction func AaswadaButtonAction(_ sender: Any) {
        UIAlertController().showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Coming Soon..!")
    }
    
    /** This Button action will trigger the function that will help to navigate to **Dhwani Screen**. */
    @IBAction func DhwaniButtonAction(_ sender: Any) {
        DispatchQueue.global(qos: .background).async {
            // Call your background task
            DispatchQueue.main.async {
                // UI Updates here for task complete.
                self.output.navigateMindfulMovementScreen()
                
                
            }
        }
    }
    
    /** This Button action will trigger the function that will help to pops up **Disclaimer View**. */
    @IBAction func settingAction(_ sender: Any) {
        disclaimer = DisclaimerView.initWith(title: "Disclaimer", filePath: "Disclaimer", fileExtension: "txt", delegate: self)
        self.view.addSubview(disclaimer)
    }
    
    /** This Method will help to hide the **DisclamierView**. */
    func cancelDisclaimer() {
        disclaimer.removeFromSuperview()
        
    }
    
    /** This Button action will trigger the function that will help to navigate to **Dhyana Screen**. */
    @IBAction func dhyanaButtonAction(_ sender: Any) {
        DispatchQueue.global(qos: .background).async {
            // Call your background task
            DispatchQueue.main.async {
                // UI Updates here for task complete.
                self.output.navigateDhyanaScreen()
                
                
            }
        }
    }
    
    /** This Button action will trigger the function that will help to navigate to Prana Screen. */
    @IBAction func pranaButtonAction(_ sender: Any) {
        
        DispatchQueue.global(qos: .background).async {
            // Call your background task
            DispatchQueue.main.async {
                // UI Updates here for task complete.
                self.output.navigatePranaScreen()
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /** This Method will help to customise the Storyboard view to all screen sizes . */
    func addSubView() {
        let modelName = UIDevice.modelName
        if modelName == "Simulator iPhone 5s" || modelName == "iPhone 5s" || modelName == "Simulator iPhone SE" || modelName == "iPhone SE" || modelName == "Simulator iPhone 5c" || modelName == "iPhone 5c"{
            self.welcomeLabelTopMargin.constant = 12.0
            self.welcomeLabel.font = UIFont(name: "OpenSans", size: 19.0)
            self.ButtonStackViewBottomMargin.constant = 80.0
            self.logoImageTopMargin.constant = 40
            self.pranaButtonWidth.constant = 48.0
            self.pranaButtonHeight.constant = 48.0
            self.dhanaButtonWidth.constant = 48.0
            self.dhanaButtonWidth.constant = 48.0
            self.dwaniButtonWidth.constant = 48.0
            self.dwaniButtonWidth.constant = 48.0
            self.aaswadaButtonWidth.constant = 48.0
            self.aaswadaButtonHeight.constant = 48.0
            
        } else if modelName == "Simulator iPhone 6" || modelName == "iPhone 6" || modelName == "Simulator iPhone 6s" || modelName == "iPhone 6s" || modelName == "Simulator iPhone 7" || modelName == "iPhone 7" || modelName == "Simulator iPhone 8" || modelName == "iPhone 8" {
            self.welcomeLabelTopMargin.constant = 14.0
            self.logoImageTopMargin.constant = 60
            self.welcomeLabel.font = UIFont(name: "OpenSans", size: 20.0)
            self.ButtonStackViewBottomMargin.constant = 65.0
            self.appIconWidth.constant = 55.0
            self.appIconHeight.constant = 55.0
            self.settingButtonWidth.constant = 32
            self.settingButtonheight.constant = 32
        } else if modelName == "Simulator iPhone 8 Plus" || modelName == "iPhone 8 Plus" || modelName == "Simulator iPhone 7 Plus" || modelName == "iPhone 7 Plus" || modelName == "Simulator iPhone 6 Plus" || modelName == "iPhone 6 Plus" || modelName == "Simulator iPhone 6s Plus" || modelName == "iPhone 6s Plus" {
            self.welcomeLabelTopMargin.constant = 14.0
            self.logoImageTopMargin.constant = 74
            self.welcomeLabel.font = UIFont(name: "OpenSans", size: 21.0)
            self.ButtonStackViewBottomMargin.constant = 70.0
            self.pranaButtonWidth.constant = 64.0
            self.pranaButtonHeight.constant = 64.0
            self.dhanaButtonWidth.constant = 64.0
            self.dhanaButtonHeight.constant = 64.0
            self.dwaniButtonWidth.constant = 64.0
            self.dwaniButtonHeight.constant = 64.0
            self.aaswadaButtonWidth.constant = 64.0
            self.aaswadaButtonHeight.constant = 64.0
            self.appIconWidth.constant = 60.0
            self.appIconHeight.constant = 60.0
            self.settingButtonWidth.constant = 35
            self.settingButtonheight.constant = 35
        } else if modelName == "Simulator iPhone X" || modelName == "iPhone X" {
            self.logoImageTopMargin.constant = 80
            self.appIconWidth.constant = 55.0
            self.appIconHeight.constant = 55.0
            self.ButtonStackViewBottomMargin.constant = 70.0
            self.settingButtonWidth.constant = 35
            self.settingButtonheight.constant = 35
        } else if modelName == "Simulator iPhone XR" || modelName == "iPhone XR" {
            self.welcomeLabelTopMargin.constant = 14.0
            self.logoImageTopMargin.constant = 94
            self.welcomeLabel.font = UIFont(name: "OpenSans", size: 22.0)
            self.ButtonStackViewBottomMargin.constant = 80.0
            self.pranaButtonWidth.constant = 68.0
            self.pranaButtonHeight.constant = 68.0
            self.dhanaButtonWidth.constant = 68.0
            self.dhanaButtonHeight.constant = 68.0
            self.dwaniButtonWidth.constant = 68.0
            self.dwaniButtonHeight.constant = 68.0
            self.aaswadaButtonWidth.constant = 68.0
            self.aaswadaButtonHeight.constant = 68.0
            self.appIconWidth.constant = 60.0
            self.appIconHeight.constant = 60.0
            self.settingButtonWidth.constant = 35
            self.settingButtonheight.constant = 35
        } else if modelName == "Simulator iPhone XS" || modelName == "iPhone XS" {
            self.logoImageTopMargin.constant = 70
            self.appIconWidth.constant = 55.0
            self.appIconHeight.constant = 55.0
            self.ButtonStackViewBottomMargin.constant = 75.0
            self.settingButtonWidth.constant = 35
            self.settingButtonheight.constant = 35
        } else if modelName ==  "Simulator iPhone XS Max" || modelName ==  "iPhone XS Max" {
            self.welcomeLabelTopMargin.constant = 14.0
            self.logoImageTopMargin.constant = 90
            self.ButtonStackViewBottomMargin.constant = 75.0
            self.pranaButtonWidth.constant = 70.0
            self.pranaButtonHeight.constant = 70.0
            self.dhanaButtonWidth.constant = 70.0
            self.dhanaButtonHeight.constant = 70.0
            self.dwaniButtonWidth.constant = 70.0
            self.dwaniButtonHeight.constant = 70.0
            self.aaswadaButtonWidth.constant = 70.0
            self.aaswadaButtonHeight.constant = 70.0
            self.appIconWidth.constant = 65.0
            self.appIconHeight.constant = 65.0
            self.settingButtonWidth.constant = 38
            self.settingButtonheight.constant = 38
        } else {
            self.welcomeLabel.font = UIFont(name: "OpenSans", size: 30.0)
            self.welcomeLabelTopMargin.constant = 20.0
            self.logoImageTopMargin.constant = 110
            self.ButtonStackViewBottomMargin.constant = 100.0
            self.pranaButtonWidth.constant = 90.0
            self.pranaButtonHeight.constant = 90.0
            self.dhanaButtonWidth.constant = 90.0
            self.dhanaButtonHeight.constant = 90.0
            self.dwaniButtonWidth.constant = 90.0
            self.dwaniButtonHeight.constant = 90.0
            self.aaswadaButtonWidth.constant = 90.0
            self.aaswadaButtonHeight.constant = 90.0
            self.appIconWidth.constant = 85.0
            self.appIconHeight.constant = 85.0
            self.settingButtonWidth.constant = 45
            self.settingButtonheight.constant = 45
        }
        
    }
}

