import UIKit

@objc class HomeModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = HomeControllerFromStoryboard()
        
        let router = HomeRouter()
        router.viewController = viewController
        
        let presenter = HomePresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = HomeInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func HomeControllerFromStoryboard() -> HomeViewController {
        let storyboard = mainStoryboard();
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.storyboard_home_vc_id)
        return viewController as! HomeViewController
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.storyboard_mainmodule, bundle: Bundle.main)
        return storyboard
    }
    
}
