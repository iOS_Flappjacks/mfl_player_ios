import UIKit

@objc class SplashModuleBuilder: NSObject {
    
    func build() -> UIViewController {
        let viewController = SplashControllerFromStoryboard()
        
        let router = SplashRouter()
        router.viewController = viewController
        
        let presenter = SplashPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let interactor = SplashInteractor()
        interactor.output = presenter
        presenter.interactor = interactor
        viewController.output = presenter
        
        //let storage = StorageService()
        //interactor.storage = storage
        
        //let net = NetService()
        //interactor.net = net
        
        presenter.configureModule()
        
        return viewController
    }
    
    func SplashControllerFromStoryboard() -> SplashViewController {
        let storyboard = mainStoryboard();
        let viewController = storyboard.instantiateViewController(withIdentifier: Constants.storyboard_splash_vc_id)
        return viewController as! SplashViewController
    }
    
    func mainStoryboard() -> UIStoryboard {
        let storyboard = UIStoryboard.init(name: Constants.storyboard_startup, bundle: Bundle.main)
        return storyboard
    }
    
}
