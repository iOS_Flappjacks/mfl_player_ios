import Foundation

class SplashPresenter: SplashModuleInput, SplashViewOutput, SplashInteractorOutput {
    
    weak var view: SplashViewInput!
    var interactor: SplashInteractorInput!
    var router: SplashRouterInput!
    
    // MARK: - SplashViewOutput
    
    func back() {
        router.back()
    }
    
    // MARK: - SplashModuleInput
    
    func configureModule() {
        
    }
    ///This function is used to navigate to home screen.
    func navigateHomeScreen(){
        router.navigateHomeScreen()
    }

}
