import Foundation

protocol SplashViewOutput {
    /** This method will trigger the pop to previous screen*/
    func back()
    /** This method will help to push from presnt screen to Home screen. */
    func navigateHomeScreen()
}
