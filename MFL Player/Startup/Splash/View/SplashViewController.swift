import UIKit

class SplashViewController: UIViewController, SplashViewInput {

    var output: SplashViewOutput!
    ///This property network connectivity status.
    let network: ConnectivityManager = ConnectivityManager.shared
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
     /** This Button action will trigger the function that will help to navigate to homescreen. */
    @IBAction func homeButtonAction(_ sender: Any) {
        DispatchQueue.global(qos: .background).async {
            // Call your background task
            DispatchQueue.main.async {
                // UI Updates here for task complete.
                self.output.navigateHomeScreen()
            }
        }
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
