import Foundation

protocol SplashRouterInput {
    /** This method will trigger the pop to previous screen*/
    func back()
    /** This function is used to navigate to home screen.*/
    func navigateHomeScreen()

}
