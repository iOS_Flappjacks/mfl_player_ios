import UIKit

class SplashRouter:SplashRouterInput {
    
    weak var viewController: SplashViewController!
    
    var rootRouter: RootRouter!
    
    func presentFromWindow(window:UIWindow) {
        /**This Method will allow to navigate from App delegate to splash viewcontroller as initial viewcontroller.
         */
        let splashModuleBuilder = SplashModuleBuilder()
        let splashViewController = splashModuleBuilder.build()
        rootRouter.showRootViewController(vc: splashViewController, window: window)
    }
    
    ///This function is used to navigate to home screen.
    func navigateHomeScreen() {
        /**This Method create in UIViewcontroller+extension class will allow to navigate to Home viewcontroller.
         */
        commonNavigateHomeScreen(vc: viewController)
    }
    
    /** This method will trigger the pop to previous screen*/
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }    
}
