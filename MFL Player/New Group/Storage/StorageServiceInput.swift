import Foundation

protocol StorageServiceInput: class {
   
    /**
     This function is used to save a value for key in User Defaults.
     - Parameter key: This property holds key.
     - Parameter value: This property holds value to be saved.
     */
    func addValue(value : Any, for key : String)
    
    /**
     This function is used to get a value for key in User Defaults.
     - Parameter key: This property holds key.
     */
    func getValueFor(key : String) -> Any
}
