import Foundation

class StorageService: StorageServiceInput {
    /// This property holds Userdefaults Object.
    let userDefaults = UserDefaults.standard
    
    // MARK: - StorageServiceInput
    /**
     This function is used to save a value for key in User Defaults.
     - Parameter key: This property holds key.
     - Parameter value: This property holds value to be saved.
     */
    func addValue(value : Any, for key : String) {
        userDefaults.setValue(value, forKey: key)
    }
    
    /**
     This function is used to get a value for key in User Defaults.
     - Parameter key: This property holds key.
     */
    func getValueFor(key : String) -> Any {
        let result = userDefaults.value(forKey: key)
        if result == nil {
            return ""
        } else {
            return result!
        }
    }
    
    
    ///This function is used to remove all userdefaults.
    func removeAllUserDefaults() {
       

    }
    
}
