//
//  ModelData.swift
//  MFL Player
//
//  Created by Omkar on 3/25/19.
//

import UIKit

//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> PranaModelData <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

/** This class hold the pranadetails Model. which collects from the API response. */
class PranaModelData: NSObject {
    var album : String!
    var author : String!
    var backgroundPic : String!
    var songTitle : String!
    var enabled : String!
    var length : String!
    var thumbnail : String!
    var title : String!
    var trackId : NSNumber!
    var trackType : String!
    var videoURL : String!
    var udid : NSNumber!
    
    /**
     This method allows to add  the pranadetails Model.
     - Parameter album: holds value of album name.
     - Parameter author : holds the value of author name.
     - Parameter backgroundPic: holds value of Backaground image URL.
     - Parameter songTitle : holds the value of the Song Title.
     - Parameter enabled: holds value of status of the song to be enable/disabled.
     - Parameter length : holds the value of the song duration length.
     - Parameter thumbnail: holds value of Song Thumbnail Image.
     - Parameter title : holds the value of the Album title.
     - Parameter trackId: holds value of track ID.
     - Parameter trackType : holds the value of the track type.
     - Parameter videoURL: holds value of Video song url.
     - Parameter udid : holds the value of the unique song number.
     */
    
    func addPranaDetailswith(album:String,author:String,backgroundPic:String,songTitle :String,enabled :String,length:String,thumbnail:String,title:String,trackId:NSNumber,trackType:String,videoURL:String,udid:NSNumber) -> PranaModelData {
        
        self.album = album
        self.author = author
        self.backgroundPic = backgroundPic
        self.songTitle = songTitle
        self.enabled = enabled
        self.length = length
        self.thumbnail = thumbnail
        self.title = title
        self.trackId = trackId
        self.trackType = trackType
        self.videoURL = videoURL
        self.udid = udid
        return self
        
    }
}

//>>>>>>>>>>>>>>>>>>>>>>>>> MindfulMovementModelData <<<<<<<<<<<<<<<<<<<<<<<<<<

/** This class hold the pranadetails Model. which collects from the API response. */
class MindfulMovementModelData: NSObject {
    var album : String!
    var author : String!
    var backgroundPic : String!
    var songTitle : String!
    var enabled : String!
    var length : String!
    var thumbnail : String!
    var title : String!
    var trackId : NSNumber!
    var trackType : String!
    var AudioURL : String!
    var udid : NSNumber!
    //var rating : String!
    
    /**
     This method allows to add  the Mindful Movement details Model.
     - Parameter album: holds value of album name.
     - Parameter author : holds the value of author name.
     - Parameter backgroundPic: holds value of Backaground image URL.
     - Parameter songTitle : holds the value of the Song Title.
     - Parameter enabled: holds value of status of the song to be enable/disabled.
     - Parameter length : holds the value of the song duration length.
     - Parameter thumbnail: holds value of Song Thumbnail Image.
     - Parameter title : holds the value of the Album title.
     - Parameter trackId: holds value of track ID.
     - Parameter trackType : holds the value of the track type.
     - Parameter videoURL: holds value of Video song url.
     - Parameter udid : holds the value of the unique song number.
     */
    
    func addMindfulMovementDetailswith(album:String,author:String,backgroundPic:String,songTitle :String,enabled :String,length:String,thumbnail:String,title:String,trackId:NSNumber,trackType:String,AudioURL:String,udid:NSNumber//,rating:String
        ) -> MindfulMovementModelData {
        
        self.album = album
        self.author = author
        self.backgroundPic = backgroundPic
        self.songTitle = songTitle
        self.enabled = enabled
        self.length = length
        self.thumbnail = thumbnail
        self.title = title
        self.trackId = trackId
        self.trackType = trackType
        self.AudioURL = AudioURL
        self.udid = udid
        // self.rating = rating
        return self
        
    }
}

//>>>>>>>>>>>>>>>>>>>>>>>>> DhyanaModelData <<<<<<<<<<<<<<<<<<<<<<<<<<

/** This class hold the pranadetails Model. which collects from the API response. */
class DhyanaModelData: NSObject {
    var album : String!
    var author : String!
    var backgroundPic : String!
    var songTitle : String!
    var enabled : String!
    var length : String!
    var thumbnail : String!
    var title : String!
    var trackId : NSNumber!
    var trackType : String!
    var videoURL : String!
    var udid : NSNumber!
    
    /**
     This method allows to add  the Dhyana details Model.
     - Parameter album: holds value of album name.
     - Parameter author : holds the value of author name.
     - Parameter backgroundPic: holds value of Backaground image URL.
     - Parameter songTitle : holds the value of the Song Title.
     - Parameter enabled: holds value of status of the song to be enable/disabled.
     - Parameter length : holds the value of the song duration length.
     - Parameter thumbnail: holds value of Song Thumbnail Image.
     - Parameter title : holds the value of the Album title.
     - Parameter trackId: holds value of track ID.
     - Parameter trackType : holds the value of the track type.
     - Parameter videoURL: holds value of Video song url.
     - Parameter udid : holds the value of the unique song number.
     */
    
    func addDhyanaDetailswith(album:String,author:String,backgroundPic:String,songTitle :String,enabled :String,length:String,thumbnail:String,title:String,trackId:NSNumber,trackType:String,videoURL:String,udid:NSNumber) -> DhyanaModelData {
        
        self.album = album
        self.author = author
        self.backgroundPic = backgroundPic
        self.songTitle = songTitle
        self.enabled = enabled
        self.length = length
        self.thumbnail = thumbnail
        self.title = title
        self.trackId = trackId
        self.trackType = trackType
        self.videoURL = videoURL
        self.udid = udid
        return self
    }
}
