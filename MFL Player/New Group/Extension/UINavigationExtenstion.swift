//
//  UINavigationExtenstion.swift
//  MFL Player
//
//  Created by Omkar on 2/5/19.
//

import Foundation
import UIKit

/** This Method will help to navigate to Home viewcontroller.
 - Parameter vc : View Controller You can use self.*/
func commonNavigateHomeScreen(vc : UIViewController) {
    let ModuleBuilder = HomeModuleBuilder()
    let ViewController = ModuleBuilder.build()
    vc.navigationController?.pushViewController(ViewController, animated: true)
}

/** This Method will help to navigate to MindfulMovement viewcontroller.
 - Parameter vc : View Controller You can use self.*/
func commonNavigateMindfulMovementscreen(vc : UIViewController) {
    let ModuleBuilder = MindfulMovementModuleBuilder()
    let ViewController = ModuleBuilder.build()
    vc.navigationController?.pushViewController(ViewController, animated: true)
}

/** This Method will help to navigate to Mindfuleating viewcontroller.
 - Parameter vc : View Controller You can use self.
 */
func commonNavigateMindfulEatingScreen(vc : UIViewController) {
    let ModuleBuilder = MindfulEatingModuleBuilder()
    let ViewController = ModuleBuilder.build()
    vc.navigationController?.pushViewController(ViewController, animated: true)
}

/** This Method will help to navigate to Dhyana viewcontroller.
 - Parameter vc : View Controller You can use self.*/
func commonNavigateDhyanaScreen(vc : UIViewController) {
    let ModuleBuilder = DhyanaModuleBuilder()
    let ViewController = ModuleBuilder.build()
    vc.navigationController?.pushViewController(ViewController, animated: true)
}

/** This Method will help to navigate to Prana viewcontroller.
 - Parameter vc : View Controller You can use self.*/
func commonNavigatePranaScreen(vc : UIViewController) {
    let ModuleBuilder = PranaModuleBuilder()
    let ViewController = ModuleBuilder.build()
    vc.navigationController?.pushViewController(ViewController, animated: true)
}

