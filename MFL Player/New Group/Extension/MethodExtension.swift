//
//  MethodExtension.swift
//  MFL Player
//
//  Created by Omkar on 2/12/19.
//

import Foundation
import UIKit
import RSLoadingView

var loadingView : RSLoadingView!
extension UIViewController {
    
    /** This function will help to load the loading animation on the view */
    func showLoading() {
        loadingView = RSLoadingView(effectType: RSLoadingView.Effect.twins)
        loadingView.showOnKeyWindow()
    }
    
    /** This function will help to hide the loading animation on the view */
    func hideLoading() {
        RSLoadingView.hideFromKeyWindow()
    }
}

extension UIAlertController {
    /**
     Call this function for showing alert with OK and Cancel button in your View Controller class.
     - Parameters:
     1) vc : View Controller over which the function is called. You can use self, or provide view controller name.
     2) titleStr : pass your alert title in string.
     3) messageStr: Pass your alert message in String.
     4) okClickHandler: This will give you call back inside block when OK button is clicked
     
     ### Usage Example: ###
     ````
     AlertClass().showAlert(self, title:"Alert" and Message: "This is custom alert") { (okClick) in
     }
     ````
     */
    func showAlertMessage(vc: UIViewController, titleStr:String, messageStr:String) -> Void {
        
        let alert = UIAlertController(title: titleStr, message: messageStr, preferredStyle: UIAlertController.Style.alert);
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    /**
     This function is used to navigate from App to device setting to enable wifi .
     */
    func internetsetting(vc: UIViewController) {
        hideLoading()
        
        let alertController = UIAlertController (title: "Mobile Data is Turned off", message: "Turn on mobile data or use Wi-Fi to access data.", preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Settings", style: .destructive) { (_) -> Void in
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        }
        alertController.addAction(settingsAction)
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
}

extension UIView{
    /**
     This function is used to show gradient color for video screen cell selection.
     */
    func addGradientBackground(firstColor: UIColor, secondColor: UIColor){
        clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}

extension Date {
    /**
     This function is used device time to unix timestamp will used to for API extension.
     
     **Return**: int value.
     */
    func toMillis() -> Int64! {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
}

extension UIView {
    /**
     This function is used for Tap gesture for rating view to disappear.
     */
    func addTapGesture(tapNumber: Int, target: Any, action: Selector) {
        let tap = UITapGestureRecognizer(target: target, action: action)
        tap.numberOfTapsRequired = tapNumber
        addGestureRecognizer(tap)
        isUserInteractionEnabled = true
    }
}
