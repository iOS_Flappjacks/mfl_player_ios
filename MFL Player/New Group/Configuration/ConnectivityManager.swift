//
//  ConnectivityManager.swift
//  LagaikhaiPro
//
//  Created by Akhil on 7/25/18.
//  Copyright © 2018 LagikhaiPro. All rights reserved.
//

import Foundation
import Reachability

class ConnectivityManager : NSObject {
    
    /// This property holds rechability object
    var reachability: Reachability!
    
    /// Creates a singleton instance
    static let shared : ConnectivityManager = { return ConnectivityManager() }()
    
    
    override init() {
        super.init()
        
        /// Initialise reachability
        reachability = Reachability()!
        
        /// Register an observer for the network status
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: .reachabilityChanged,
            object: reachability
        )
        
        do {
            // Start the network status notifier
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    @objc func networkStatusChanged(_ notification: Notification) {
        // Do something globally here!
    }
    
    static func stopNotifier() -> Void {
        do {
            // Stop the network status notifier
            try (ConnectivityManager.shared.reachability).startNotifier()
        } catch {
            print("Error stopping notifier")
        }
    }
    
    /// Network is reachable
    static func isReachable(completed: @escaping (ConnectivityManager) -> Void) {
        if (ConnectivityManager.shared.reachability).connection != .none {
            completed(ConnectivityManager.shared)
        }
    }
    
    /// Network is unreachable
    static func isUnreachable(completed: @escaping (ConnectivityManager) -> Void) {
        if (ConnectivityManager.shared.reachability).connection == .none {
            completed(ConnectivityManager.shared)
        }
    }
    
    /// Network is reachable via WWAN/Cellular
    static func isReachableViaWWAN(completed: @escaping (ConnectivityManager) -> Void) {
        if (ConnectivityManager.shared.reachability).connection == .cellular {
            completed(ConnectivityManager.shared)
        }
    }
    
    /// Network is reachable via WiFi
    static func isReachableViaWiFi(completed: @escaping (ConnectivityManager) -> Void) {
        if (ConnectivityManager.shared.reachability).connection == .wifi {
            completed(ConnectivityManager.shared)
        }
    }
}

