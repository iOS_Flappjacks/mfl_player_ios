import Foundation
import UIKit

/**This class will help to use constants globally. Storyboard ID's, Viewcontroller ID's
 */
class Constants {
    ///This property knows start up storyboard Identifier.
    static let storyboard_startup = "StartUp"
    ///This property knows main storyboard Identifier.
    static let storyboard_mainmodule = "MainModule"
    ///This property knows splash view controller storyboard Identifier.
    static let storyboard_splash_vc_id = "SplashViewController"
    ///This property knows home view controller storyboard Identifier.
    static let storyboard_home_vc_id = "HomeViewController"
    ///This property knows MindfulMovement view controller storyboard Identifier.
    static let storyboard_mindfulmovement_vc_id = "MindfulMovementViewController"
    ///This property knows MindfulEating view controller storyboard Identifier.
    static let storyboard_mindfuleating_vc_id = "MindfulEatingViewController"
    ///This property knows Dhyana view controller storyboard Identifier.
    static let storyboard_dhyana_vc_id = "DhyanaViewController"
    ///This property knows Prana view controller storyboard Identifier.
    static let storyboard_prana_vc_id = "PranaViewController"
 
}

/**This class will holds to Device model name constants globally.
 */
class DeviceConstants {
    ///This property knows iPhone 5 device Key.
    static let iPHONE_5 = "iPhone 5/5S"
    ///This property knows iPhone 6 device Key.
    static let iPHONE_6 = "iPhone 6/6s/7/8"
    ///This property knows iPhone plus device Key.
    static let iPHONE_6_ABOVE = "iPhone 6+/7+/8+"
    ///This property knows iPhone x device Key.
    static let iPHONE_X = "iPhone X"
    ///This property knows iPhone XR device Key.
    static let iPHONE_XR = "iPhone XR"
    ///This property knows iPhone XR device Key.
    static let iPHONE_XS_MAX = "iPhone XS_MAX"
    ///This property knows ipad device Key.
    static let IPAD  = "iPad"

}

/**This class will help to use Images constants globally.
 */
class ImageConstants {
    static let PAUSE_IMAGE = "PauseButton"
    static let PLAY_IMAGE = "PlayButton"
    static let GRADENT_SELECTED_BACKGROUNDIMAGE = "GradientSelectedimage"
    static let SLIDER_THUMB_IMAGE = "sliderimage"

}

/**This class will help to use XIB's constants globally.
 */
class NibNameConstants {
    ///This property knows VideoTableViewCell identifier.
    static let VIDEOSLIST_TABLEVIEWCELL = "VideoTableViewCell"
    ///This property knows Mindful movement collectionview cell identifier.
    static let MINDFULMOVEMENT_COLLECTIONVIEW_CELL = "MindfulMovementCollectionViewCell"
}


class Defaults {
    /** This method will help to save Rating details and used for globally.
    - Parameters:
      value = array of static array is saved.
      key = name of the key with string.
     */
    func saveRatingDetails(value : NSMutableArray, key : String ){
        do {
            let ratingData = try NSKeyedArchiver.archivedData(withRootObject: value, requiringSecureCoding: true)
            UserDefaults.standard.set(ratingData, forKey: key)
        }  catch {
            print("Unexpected error: \(error).")
        }
    }
    
    func loadRatingDetailsFor(key : String) -> NSMutableArray {
        let ratingDetails = NSMutableArray()
        guard let ratingData = UserDefaults.standard.object(forKey: key) as? NSData else {
            print("'ratingData' not found in UserDefaults")
            return []
        }
        
        do {
            let ratingDetailsArray = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(ratingData as Data) as? NSMutableArray
            for detail in ratingDetailsArray! {
                ratingDetails.add(detail)
            }
        } catch {
            print("Unexpected error: \(error).")
            return []
        }
        return ratingDetails
    }

}


