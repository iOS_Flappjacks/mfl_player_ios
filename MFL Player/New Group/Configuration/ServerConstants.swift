//
//  ServerConstants.swift
//  MFL Player
//
//  Created by Omkar on 2/14/19.
//

import Foundation

let currentTimeStamp = Date().toMillis()

///This class holds base url constants.
class UrlConstants {
    ///This property holds  base url path of application.
    static let BASE_URL = "https://thefullerlife.com/mfl/"
    ///This property holds production MindFulMovementAPI url path of application.
    static let MINDFUL_MOVEMENT_API = BASE_URL + "mindful_movement_track_list.php/" + String(describing: currentTimeStamp)
    ///This property holds production DhyanaAPI url path of application.
    static let DHYANA_API = BASE_URL + "dhyana_track_list.php/" + String(describing: currentTimeStamp)
    ///This property holds production PranaAPI url path of application.
    static let PRANA_API = BASE_URL + "prana_track_list.php/" + String(describing: currentTimeStamp)
}

///This class holds API Response keys.
class protocolKeyConstants {
     static let RESPONSE_TRACK_LIST = "track_list"
     static let RESPONSE_ALBUM = "album"
     static let RESPONSE_AUTHOR = "author"
     static let RESPONSE_BACKGROUND_PIC = "background_pic"
     static let RESPONSE_DESCRIPTION = "description"
     static let RESPONSE_ENABLED = "enabled"
     static let RESPONSE_LENGTH = "length"
     static let RESPONSE_THUMBNAIL = "thumbnail"
     static let RESPONSE_TITLE = "title"
     static let RESPONSE_TRACK_ID = "track_id"
     static let RESPONSE_TRACK_TYPE = "track_type"
     static let RESPONSE_URL = "url"
     static let RESPONSE_UDID = "uuid"
}
