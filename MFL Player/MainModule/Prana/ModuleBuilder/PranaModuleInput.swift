import Foundation

protocol PranaModuleInput: class {
    
    func configureModule()
}
