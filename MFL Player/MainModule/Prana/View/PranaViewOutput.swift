import Foundation

protocol PranaViewOutput {
    func back()
    func hitPranaAPI()
}
