//
//  VideoTableViewCell.swift
//  MFL Player
//
//  Created by Omkar on 2/11/19.
//

import UIKit

protocol VideoCellDelegate {
    func navigateToRateView(buttonIndex : Int)
}
class VideoTableViewCell: UITableViewCell {

    @IBOutlet weak var videoThumbnailImage:UIImageView!
    @IBOutlet weak var title:UILabel!
    @IBOutlet weak var timeTitle:UILabel!
    @IBOutlet weak var downloadButton:UIButton!
    @IBOutlet weak var custombackgroundview:UIView!
    @IBOutlet weak var contentview:UIView!
    @IBOutlet weak var ratingButton : UIButton!
    @IBOutlet weak var ratinglabelTitle:UILabel!
    var delegate : VideoCellDelegate!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /** This private method help to keep gradient colors.
     - Parameter:
        1) redcolor = value for 255.0
        2) bluecolor = value for 255.0
        3) greencolor = value for 255.0
        4) alpha = value for 1.0
     
     - Return: gradient color.
     */
    private lazy var gradient: CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor (red: 119/255.0, green:91/255.0 , blue: 166/255.0, alpha:1.0).cgColor, UIColor (red: 48/255.0, green: 190/255.0 , blue: 213/255.0, alpha:1.0 ).cgColor]
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = self.bounds
        return gradientLayer
    }()

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if self.isSelected
        {
            self.custombackgroundview.layer.insertSublayer(self.gradient, at: 0)
        }
        else
        {
            self.gradient.removeFromSuperlayer()
        }

    }
   
    /** This Button action will trigger the function that pass the delegate method to main class. */
    @IBAction func rateactionNavigate(_ sender: UIButton) {
        
        self.delegate.navigateToRateView(buttonIndex: sender.tag)
    }
}
