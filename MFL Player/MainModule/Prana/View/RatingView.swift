//
//  DisclaimerView.swift
//  ScrollableDisclaimerViewAlert
//
//  Created by Kuba on 09/03/2017.
//  Copyright © 2017 Jakub Cizek. All rights reserved.
//

import UIKit
import FloatRatingView

/** This delegate protocol  for rating view to get rating and also closing ratingview*/
protocol RatingViewDelegate: class {
    func noThanks()
    func rated(ratingValue:String!)
}

class RatingView: UIView,FloatRatingViewDelegate {
    let kAnimationDuration = 0.3
    // MARK: Delegate
    weak var delegate: RatingViewDelegate?
    ///This property holds song title label.
    @IBOutlet weak var songTitle: UILabel!
    ///This property holds rating BackGroundView object.
    @IBOutlet var ratingBGView : UIView!
    ///This property holds rating View label.
    @IBOutlet var ratingView : FloatRatingView!
    ///This property holds scontainer View object.
    @IBOutlet weak var containerView:UIView!

    var ratingValue : String!
    
    // MARK: Implementation
    
   /** This class method will generate rating view from Nibfile. */
    class func initView() -> RatingView {
        
        let ratingView = UINib(nibName: "RatingView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! RatingView
        return ratingView
    }
    
    /**
     This method allows to create rating view.
     - Parameter title: holds value of album name.
     - Parameter presentRating : holds the current rating value of particular song.
     - Parameter delegate: holds rating delegate.
     - Return Ratingview
     */
    class func initWith(title: String,presentRating:String, delegate: RatingViewDelegate) -> RatingView {
        
        let ratingPopUpView = UINib(nibName: "RatingView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! RatingView
        ratingPopUpView.delegate = delegate
        ratingPopUpView.frame = UIScreen.main.bounds
        ratingPopUpView.songTitle.text = title
        ratingPopUpView.ratingView.delegate = ratingPopUpView.self
        ratingPopUpView.ratingView.rating = Double(presentRating)!
        ratingPopUpView.ratingView.contentMode = UIView.ContentMode.scaleAspectFit
        
        return ratingPopUpView
    }
    
    /** This method will create the background layer for rating view.*/
    override func layoutSubviews() {
        layer.opacity = 0
        UIView.animate(withDuration: kAnimationDuration) {
            self.layer.opacity = 1
        }
    }
    
    /** This button action will help to close the rating view .*/
    @IBAction func cancelAction(_ sender: Any) {
        delegate?.noThanks()
    }
    
    /** This method will helps to remove rating view from superview .*/
    override func removeFromSuperview() {
        UIView.animate(withDuration: kAnimationDuration, animations: {
            self.layer.opacity = 0
        }) { processing in
            if !processing { super.removeFromSuperview() }
        }
    }
    
    /** This method will help update rating and UI .*/
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        self.ratingValue = String(describing:Int(ratingView.rating) )
        delegate?.rated(ratingValue:ratingValue)
    }
  
    }

