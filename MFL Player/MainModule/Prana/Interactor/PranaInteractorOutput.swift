import Foundation

protocol PranaInteractorOutput: class {
   
/**
 This method return the response to Update UI
 - Parameter result : holds NSDictionary. */
    func responsePranaApI(result:NSDictionary)

}
