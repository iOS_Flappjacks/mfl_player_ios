//
//  DhyanaInteractor.swift
//  MFL Player
//
//  Created by Omkar on 2/8/19.
//  Copyright 2019 ___ORGANIZATIONNAME___. All rights reserved.
//
// -----------------------------------------------------------------



import Foundation

class DhyanaInteractor: DhyanaInteractorInput {
    
    weak var output: DhyanaInteractorOutput!
    var net = NetService()

    /** This method will help to hit the DhyanaAPI and return the response. */
    func hitDhyanaApi()
    {
        net.hitGetAPIWith(url: UrlConstants.DHYANA_API) { (value) in
            let result = value as NSDictionary?
            self.output.responseDhyanaApI(result : result  ?? [:])
        }
    }
}
