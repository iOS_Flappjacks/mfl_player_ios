import Foundation

protocol DhyanaInteractorOutput: class {
    
    /**
     This method return the response to Update UI
     - Parameter result : holds NSDictionary. */
    func responseDhyanaApI(result:NSDictionary)
}
