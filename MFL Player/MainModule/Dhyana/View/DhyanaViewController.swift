import UIKit
import AVKit
import AVFoundation
import AlamofireImage

/**
 This class basically implemented for Dhyana Video playing screen.
 1) It handled video playing from the remote URL. which is trigger from the API response.
 2) Video Player is build the framework like AVfoundation.
 3) Player is consisting of forward, previous,pause,start controls with Audio adjustable slider controller.
 4) Rating feature implemented for individual song on the scale of 1-5.
 */
class DhyanaViewController: UIViewController, DhyanaViewInput,VideoCellDelegate,RatingViewDelegate {

    ///This property holds video Player Background View .
    @IBOutlet weak var videoPlayerView : UIView!
    ///This property holds videoListTableView  .
    @IBOutlet weak var videoListTableView : UITableView!
    var output: DhyanaViewOutput!
    ///This property holds network connectivity status.
    let network: ConnectivityManager = ConnectivityManager.shared
    ///This property holds rating view intialisation.
    var rating = RatingView.initView()
    ///This property holds player instance.
    var player:AVPlayer?
    ///This property holds player controller.
    let controller = AVPlayerViewController()
    ///This property holds Dhyana Songs array.
    let DhyanaArray = NSMutableArray()
    ///This property holds rating '0'initialising array.
    var ratingArraysetup = NSMutableArray()
    ///This property holds rating array.
    var ratingArray = NSMutableArray()
    ///This property tableview current Index.
    var buttonIndex = Int()
    ///This property Song Current Index.
    var index :Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        showLoading()
        registertableViewcell()
        rating.ratingBGView.addTapGesture(tapNumber: 1, target: self, action: #selector(hideview))

        /**  NotificationCenter addObserver will help to trigger. when app on foreground and help to refresh the app content along with internetcheck.*/
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(playerItemDidReachEnd(notification:)),
                                               name: .AVPlayerItemDidPlayToEndTime,
                                               object: player?.currentItem)

    }
    
    /** This function is used to register CollectionViewCell.
- ## UI SetUp :
     1) Registering nib file is requied while collection view created in xib.
     */
    func registertableViewcell() {
        self.videoListTableView.register(UINib.init(nibName: NibNameConstants.VIDEOSLIST_TABLEVIEWCELL , bundle: nil), forCellReuseIdentifier: NibNameConstants.VIDEOSLIST_TABLEVIEWCELL)
    }
    
    /** This function will help to trigger every time view is appear */
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.checkInternetConnetionStatus()
    }
    
    /** This function will help to trigger every time view is disappear */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        player?.pause()
    }
    
    /** This function will help to trigger when song reached to end and open the rating view
-## Logical SetUp:
     1) when player came to reach to end point of duration .This method will triggered.
     2) once song is completed it will be closed automatically and after 2 second delay rating view will be pops up .
    
     */
    @objc func playerItemDidReachEnd(notification: Notification) {
        if (notification.object as? AVPlayerItem) != nil {
                if self.ratingArray.object(at: self.index) as! String == "0" {
                    DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                        self.navigateToRateView(buttonIndex: self.index )
                        
                    })
                }
                else {
                }
            }
        }
    
    /** This function get the status of rechability.
- ## Logical SetUp :
     1) ConnectivityManager.isReachable / network.reachability.whenReachable will indicates their is internet connection in device.
     2) ConnectivityManager.isUnreachable / network.reachability.whenUnreachable will indicates their is no internet connection in device.
     */
    func checkInternetConnetionStatus() {
        
        ConnectivityManager.isReachable { networkManagerInstance in
            self.output.hitDhyanaApi()
        }
        
        ConnectivityManager.isUnreachable { networkManagerInstance in
             UIAlertController().internetsetting(vc: self)
        }
        
        network.reachability.whenReachable = { _ in
            DispatchQueue.main.async {
            }
        }
        
        network.reachability.whenUnreachable = { _ in
            DispatchQueue.main.async {
                UIAlertController().internetsetting(vc: self)
            }
        }
    }

    /**This function that will carry the response from API and will assign to model class and reload the view with new data.
- parameter result : It holds the dictionary value from API response.
- ## Logical SetUp :
     1) From the response **Result**. We will collect all value in model data format and will add each model data into a array.
     2) Array will be cleared before adding the new data to avoid the duplicate data.
     3) As we are not rating from the Json response need to add rating manual . if rating is nil/zero and will be saved in userdefaults.
     4) Tableview will reloaded with new data.
     */
    func responseDhyanaApI(result:NSDictionary){
        
        guard let tracklist = result[protocolKeyConstants.RESPONSE_TRACK_LIST] as? NSArray else { return }
        DhyanaArray.removeAllObjects()
        for detail in tracklist {
            let details = detail as! NSDictionary
            let modalClass = DhyanaModelData().addDhyanaDetailswith(
                album: (details.object(forKey: protocolKeyConstants.RESPONSE_ALBUM)as? String ?? "")!,
                author: (details.object(forKey: protocolKeyConstants.RESPONSE_AUTHOR)as? String ?? "")!,
                backgroundPic: (details.object(forKey: protocolKeyConstants.RESPONSE_BACKGROUND_PIC)as? String ?? "")!,
                songTitle: (details.object(forKey: protocolKeyConstants.RESPONSE_DESCRIPTION)as? String ?? "")!,
                enabled: (details.object(forKey: protocolKeyConstants.RESPONSE_ENABLED)as? String ?? "")!,
                length: (details.object(forKey: protocolKeyConstants.RESPONSE_LENGTH)as? String ?? "")!,
                thumbnail: (details.object(forKey: protocolKeyConstants.RESPONSE_THUMBNAIL)as? String ?? "")!,
                title: (details.object(forKey: protocolKeyConstants.RESPONSE_TITLE)as? String ?? "")!,
                trackId: (details.object(forKey: protocolKeyConstants.RESPONSE_TRACK_ID)as? NSNumber ?? 0)!,
                trackType: (details.object(forKey: protocolKeyConstants.RESPONSE_TRACK_TYPE)as? String ?? "")!,
                videoURL: (details.object(forKey: protocolKeyConstants.RESPONSE_URL)as? String ?? "")!,
                udid: (details.object(forKey: protocolKeyConstants.RESPONSE_UDID)as? NSNumber ?? 0)!)
            
            DhyanaArray.add(modalClass)
            ratingArraysetup.add("0")
            
        }
        let data : Any? = Defaults().loadRatingDetailsFor(key: "DhyanaRatingArray")
        if data == nil {
            
        } else {
            ratingArray = data as! NSMutableArray
        }
        if ratingArraysetup.count != ratingArray.count {
            for _ in ratingArray.count..<ratingArraysetup.count{
                ratingArray.add ("0")
            }
            Defaults().saveRatingDetails(value: ratingArray, key: "DhyanaRatingArray")
        } else {
            
            
        }
        videoListTableView.reloadData()
        self.setUPVideoPlayer()
        hideLoading()
    }
    
    /** This function will help to setup video player with data from server and allow play when device in mute mode.
- ## Logical Setup
     1) setUpvideoPlayerPlay will handle assigning of remote URL to player .
     2) AVAudioSession.sharedInstance().setCategory() will allow to play sound in mute mode of the device.
     3) player play will wait for user input to play.
     4) make the player plays in full screen.
     */
    func setUPVideoPlayer() {
        let videodetails = DhyanaArray.object(at: index) as! DhyanaModelData
        let videoUrl: URL = URL(string: videodetails.videoURL  )!
        player = AVPlayer(url:videoUrl)
        controller.player = player
        controller.view.frame = self.videoPlayerView.frame
        controller.entersFullScreenWhenPlaybackBegins = true
        controller.exitsFullScreenWhenPlaybackEnds = true
        self.view.addSubview(controller.view)
        self.addChild(controller)
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [.mixWithOthers, .defaultToSpeaker,.allowAirPlay])
           
            try AVAudioSession.sharedInstance().setActive(true)
        } catch {
            print(error)
        }
    }
    
 /** This function will help to setup video player with data from server and allow play when device in mute mode and automatically start playing.
- ## Logical Setup
     1) setUpvideoPlayerPlay will handle assigning of remote URL to player
     2) AVAudioSession.sharedInstance().setCategory() will allow to play sound in mute mode of the device.
     3) player play immediately with speed of 1.0.
     4) make the player plays in full screen.
     */
    func setUpvideoPlayerPlay() {
        let videodetails = DhyanaArray.object(at: index) as! DhyanaModelData
        let videoUrl: URL = URL(string: videodetails.videoURL  )!
        player = AVPlayer(url:videoUrl)
        controller.player = player
        controller.view.frame = self.videoPlayerView.frame
        controller.entersFullScreenWhenPlaybackBegins = true
        controller.exitsFullScreenWhenPlaybackEnds = true
        self.view.addSubview(controller.view)
        self.addChild(controller)
        player?.play()
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [.mixWithOthers, .defaultToSpeaker,.allowAirPlay])
            try AVAudioSession.sharedInstance().setActive(true)
       
        } catch {
            print(error)
        }
        
        enterFullscreen(playerViewController: controller)
    }
    
    /** This function will help to play video in full screen
 - parameter playerViewController: holds
 - ## Logical SetUp :
     1) when video start playing. Making transition To FullScreen Animated. Depends upon the version of iOS.
     */
    private func enterFullscreen(playerViewController: AVPlayerViewController) {
        
        let selectorName: String = {
            if #available(iOS 11.3, *) {
                return "_transitionToFullScreenAnimated:interactive:completionHandler:"
            } else if #available(iOS 11, *) {
                return "_transitionToFullScreenAnimated:completionHandler:"
            } else {
                return "_transitionToFullScreenViewControllerAnimated:completionHandler:"
            }
        }()
        let selectorToForceFullScreenMode = NSSelectorFromString(selectorName)
        
        if playerViewController.responds(to: selectorToForceFullScreenMode) {
            playerViewController.perform(selectorToForceFullScreenMode, with: true, with: nil)
        }
    }
 
    
    /**This Button action will trigger the function that will help to navigate to homescreen.
- parameter sender : UIButton.
- ## Logical SetUp :
     1) when Homebutton is triggered.Player will be paused and pops by back to previous screen.
     */
    @IBAction func homeButtonAction(_ sender: Any) {
        self.output.back()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /** This function will help open rating view and assign rating action
- ## UI SetUp :
     1) rating action will allocate the Ui will songTitle, presentRating and tap gesture.
     
     */
    func navigateToRateView(buttonIndex: Int) {
        let model = self.DhyanaArray[index] as! DhyanaModelData
        rating = RatingView.initWith(title:model.title, presentRating:ratingArray.object(at: buttonIndex) as! String,delegate: self)
        self.buttonIndex = buttonIndex
        self.view.addSubview(rating)
        rating.ratingBGView.addTapGesture(tapNumber: 1, target: self, action: #selector(hideview))
    }
    
    /** This function will helpto hide rating view */
    @objc func hideview() {
        self.rating.isHidden = true
    }
}

/**This extension is to make cleancode for the table view to the class */
extension DhyanaViewController : UITableViewDelegate,UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DhyanaArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NibNameConstants.VIDEOSLIST_TABLEVIEWCELL) as! VideoTableViewCell
        let model = self.DhyanaArray[indexPath.row] as! DhyanaModelData
        if model.songTitle == "" {
            cell.title.text = model.songTitle
            
        } else {
            cell.title.text = model.title
        }
        cell.delegate = self
        cell.ratinglabelTitle.text = ratingArray.object(at: indexPath.row) as? String
        cell.timeTitle.text = model.length
        cell.ratingButton.tag = indexPath.row
        let downloadURL = NSURL(string:model.thumbnail)!
        cell.videoThumbnailImage.af_setImage(withURL: downloadURL as URL )
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let modelName = UIDevice.modelName
        if modelName == "Simulator iPhone 5s" || modelName == "iPhone 5s" || modelName == "Simulator iPhone SE" || modelName == "iPhone SE" || modelName == "Simulator iPhone 5c" || modelName == "iPhone 5c" {
            
            return 80
        } else if modelName == "Simulator iPhone 6" || modelName == "iPhone 6" || modelName == "Simulator iPhone 6s" || modelName == "iPhone 6s" || modelName == "Simulator iPhone 7" || modelName == "iPhone 7" || modelName == "Simulator iPhone 8" || modelName == "iPhone 8" {
            return 80
        } else {
            return 90 }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        player?.pause()
        self.index = indexPath.row
        setUpvideoPlayerPlay()
    }
    
    /**This function will help to close the rating view and along with song completion check.
- ## Logical SetUp :
     1) ratingview will be removed by clicking the close button.
     2) Songcompletioncheck will let you the status of that particular song.
     */
    func noThanks() {
        self.rating.isHidden = true
    }
    
    /** This function will help to confirm the rating is allocated for that song.
     
 - parameter ratingValue : It bring the rating value between 1 - 5
- ## Logical SetUp :
     1) Ratingview will be removed once rating is given.
     2) Rating star will be updated depends upon user selection.
     3) Rating for the particular song  will be updated with new rating in the ratingArray.
     4) Rating array is saved in userdefaults for the key **DhyanaRatingArray** .
     5) Song completioncheck will help to setup next song Ui and audio for the player.
     */
    func rated(ratingValue:String!) {
        ratingArray.replaceObject(at: buttonIndex, with: ratingValue)
        Defaults().saveRatingDetails(value: ratingArray, key: "DhyanaRatingArray")
        self.videoListTableView.reloadData()
        self.rating.isHidden = true

    }
}
