import UIKit

class DhyanaRouter:DhyanaRouterInput {
    
    weak var viewController: DhyanaViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
}
