import Foundation

protocol MindfulMovementViewInput: class {
    func responseMindfulMovement(result:NSDictionary)
}
