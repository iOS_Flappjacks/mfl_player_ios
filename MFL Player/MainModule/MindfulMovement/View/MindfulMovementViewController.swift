import UIKit
import AVKit
import AVFoundation
import AlamofireImage
import MediaPlayer
import RSLoadingView

/**
 This class basically implemented for Dhwani Audio playing screen.
 1) It handled audio playing from the remote URL. which is trigger from the API response.
 2) Audio Player is build the framework like AVfoundation, Mediaplayer.
 3) Player is consisting of forward, previous,pause,start controls with Audio adjustable slider controller.
 4) Rating feature implemented for individual song on the scale of 1-5.
 */

class MindfulMovementViewController: UIViewController, MindfulMovementViewInput,RatingViewDelegate {

    var output: MindfulMovementViewOutput!
    ///This property holds audioplayer previous button.
    @IBOutlet weak var previousButton : UIButton!
    ///This property holds audioplayer next button.
    @IBOutlet weak var nextButton : UIButton!
    ///This property holds audioplayer play button.
    @IBOutlet weak var playButton : UIButton!
    ///This property holds audioplayer audioDuration label.
    @IBOutlet weak var audioDuration : UILabel!
    ///This property holds song author label.
    @IBOutlet weak var artistname : UILabel!
    ///This property holds song title label.
    @IBOutlet weak var songTitle : UILabel!
    ///This property holds song background Imageview.
    @IBOutlet weak var songBackgroundImage : UIImageView!
    ///This property holds watchplaylistView.
    @IBOutlet weak var watchPlaylistView : UIView!
    ///This property holds song upComingPlaylistView.
    @IBOutlet weak var upComingPlaylistView : UIView!
    ///This property holds song AudioPlayer Slider.
    @IBOutlet weak var audioSlider : UISlider!
    ///This property holds watchPlaylistCollectionView Left Constraint.
    @IBOutlet var watchPlaylistCollectionViewLeftConstraint : NSLayoutConstraint!
    ///This property holds watchPlaylistCollectionView Right Constraint.
    @IBOutlet var watchPlaylistCollectionViewRightConstraint : NSLayoutConstraint!
    ///This property knows playlist CollectionView.
    @IBOutlet var watchPlaylistCollectionView : UICollectionView!
    ///This property holds ratingvalue label.
    @IBOutlet var ratingvalue : UILabel!
    @IBOutlet weak var playListLabel: UILabel!
    

    ///This property initialising RatingView.
    var rating = RatingView.initView()
    ///This property holds Timer.
    var timer:Timer!
    ///This property holds audio total duration.
    var endTime : String!
    ///This property holds audio Start time.
    var startTime : String!
    ///This property holds network connectivity status.
    let network: ConnectivityManager = ConnectivityManager.shared
    ///This property holds AVplayer.
    var player:AVPlayer?
    ///This property holds AVplayerItem.
    var playerItem:AVPlayerItem?
    ///This property holds audioUrl.
    var audioUrl : URL!
    ///This property Song Current Index.
    var index :Int = 0
    ///This property holds MindfulMovement model Array.
    let MindfulMovementArray = NSMutableArray()
    ///This property holds song background operation key.
    let commandCenter = MPRemoteCommandCenter.shared()
    ///This property holds rating '0'initialising array.
    var ratingArraysetup = NSMutableArray()
    ///This property holds rating array.
    var ratingArray = NSMutableArray()
    ///This property holds decision key to display the ratingview for that song.
    var ratingDisappear = Bool()
    ///This property holds decision key to display the next button for that song.
    var nextplayaction = Bool()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showLoading()
        self.buttonSetup()
        self.backgroundModeAudioHandling()
        self.setUpRightGesture ()
        self.registerCollectionView()
        self.setUpComingView()
        /**  NotificationCenter addObserver will help to trigger. when app on foreground and help to refresh the app content along with internetcheck. */
        NotificationCenter.default.addObserver(self, selector:#selector(self.viewWillAppear), name: UIApplication.didBecomeActiveNotification, object: nil)
        
    }
    
    /**This function will help to trigger every time view is disappear */
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.view.removeFromSuperview()
        stopTimer()
    }
    
    /**This function will help to trigger every time view is appear
-## logical setup
     1) when start time is equal to nil / zero . it shows song is not started yet . will check the internet connectivity.
     2) Depending the button current image available on UI. will decide whether song is playing or paused stage.
     3) when button image is not playing image. it indicates playing. will collected the audioslider value to get the current time  and will be passed to player seek time . Song will resume from that.
     4) Timer will be triggered and pause button will be enabled.
     */
    override func viewWillAppear(_ animated: Bool)
    {

        if startTime == nil || startTime == "0" {
            self.checkInternetConnetionStatus()
        } else  {
            let btnImag = playButton.image(for: .normal)
            if btnImag == UIImage(named:ImageConstants.PLAY_IMAGE )  {
            } else  {
                
                showLoading()
                let seconds : Int64 = Int64(audioSlider.value)
                let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
                player?.seek(to: targetTime)
                player?.playImmediately(atRate: 1.0)
                startTimer()
                pauseButtonEnable()
            }
        }
        
    }
    
    /** This function will help open rating view and assign rating action
- ## UI SetUp :
     1) rating action will allocate the Ui will songTitle, presentRating and tap gesture.
     
     */
    @IBAction func ratingAction() {
        rating = RatingView.initWith(title:songTitle.text!,presentRating:ratingvalue.text! ,delegate: self)
        self.view.addSubview(rating)
        rating.ratingBGView.addTapGesture(tapNumber: 1, target: self, action: #selector(hideview))
    }
    
    
    /** This function will help to check current status of the song and trigger to next song when song is completed.
- ## Logical SetUp :
     1) when Starttime of the song is nil. It will assigned as zero secs/ orgin.
     2) when start time is equal to end time of the song duration. it indicate song completed will stop the timer.
        2a) Rating value is not zero. we restrict the ratingview not to appear.songCompleted will push to next song UiUpdation and playing.
        2b) Rating value is zero. we will check rating disappear(whether user requested for disappearing or not.) . if it is not disappeared from user. will pop up the rating view.
     3) when start time is greater the end time will make start time to equal the end time and remove the rating view.
        3a) we will check rating disappear(whether user requested for disappearing or not.) . if it is not disappeared from user. will pop up the rating view.
     */
    func songCompletioncheck() {
        if self.startTime == nil {
            startTime = "0"
        }
        if self.startTime == self.endTime  {
            stopTimer()
            if  ratingvalue.text != "0" {
                songCompleted()
            } else {
                hideLoading()
                if ratingDisappear == false {
                    self.ratingAction()
                    ratingDisappear = true
                } else {
                    songCompleted()
                }
            }
            
        }  else if self.startTime > self.endTime {
            self.startTime = self.endTime
            rating.removeFromSuperview()
            if ratingDisappear == false {
                self.ratingAction()
            } else {
                songCompleted()
            }
        }  else {
            hideLoading()
        }
    }
    
    /** This function will help check currnt song index and will push next/previous accoring to index value.
   
- ## Logical SetUp :
     1) when song completed Audio slider value is making to zero . so that it will move from orgin for next song.
     2) mutableIndex = current song index is incremented as current song is completed
     3) when mutable index is greater than songs count minus one . it indicate first song by changing index value zero.
     4) if mutable index is less than song count it navigate to next song.
    
     */
    func songCompleted() {
        audioSlider.value = 0.0
        let mutableindex = self.index + 1
        if mutableindex > self.MindfulMovementArray.count - 1 {
            self.index = 0
            self.audioSetup()
            self.playButtonEnable()
        } else {
            self.nextButtonAction((Any).self)
            nextplayaction = true
            ratingDisappear = false
            
        }
    }
    
    /** This function will helpto hide rating view
- ## UI SetUp :
     1) will hide the rating view.
     */
    @objc func hideview() {
        rating.removeFromSuperview()
    }
    
    /** This function change slider value and song time.
 - parameter sender : audio slider value
- ## Logical SetUp :
     1) when slider value is changed. slider will be converted in timeformat will be passed players seektime.
     2) player seek time will allow to play song from that particular seconds.
     3) playimmediate make player to play in speed rate at 1.0 . so it willnot required to wait till default buffer time.
     4) timer will restart  and UI will be changed as per current situation.
     
     */
    @IBAction func sliderAction(_ sender: UISlider) {
        showLoading()
        let seconds : Int64 = Int64(sender.value)
        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
        player?.seek(to: targetTime)
        player?.playImmediately(atRate: 1.0)
        startTimer()
        pauseButtonEnable()
    }
    
    /**This function will help to setup Audio player slidercontroller and create View design
- ## UI SetUp :
     1) added slider color to purple and white thumb image to tracker.
     2) Slider fixed to zero and maximum value to timer value.
     */
    func setUpCustomSlideBar() {
        audioSlider.minimumTrackTintColor = UIColor (red: 48/255.0, green: 190/255.0 , blue: 213/255.0, alpha:1.0 )
        audioSlider.setThumbImage(UIImage (named: ImageConstants.SLIDER_THUMB_IMAGE) , for: UIControl.State())
        
        let audioAsset = AVURLAsset.init(url: audioUrl!, options: nil)
        let duration = audioAsset.duration
        let seconds : Float64 = CMTimeGetSeconds(duration)
        audioSlider.minimumValue = 0
        audioSlider.maximumValue = Float(seconds)
    }
    
    
    /**This function will help to setup the set upcoming view depending on the device.
- ## UI SetUp :
     1) getting the device model name  and adjusting Ui according to device height.
     */
    func setUpComingView() {
        let modelName = UIDevice.modelName
        if modelName == "Simulator iPhone 5s" || modelName == "iPhone 5s" || modelName == "Simulator iPhone SE" || modelName == "iPhone SE" || modelName == "Simulator iPhone 5c" || modelName == "iPhone 5c"{
            self.upComingPlaylistView.frame = CGRect(x: self.upComingPlaylistView.frame.origin.x+20 , y: self.upComingPlaylistView.frame.origin.y + 360, width: self.view.frame.width-40, height: self.upComingPlaylistView.frame.height)
            watchPlaylistCollectionViewLeftConstraint.constant = 20
            watchPlaylistCollectionViewRightConstraint.constant = 20
            
            
        } else if modelName == "Simulator iPhone 6" || modelName == "iPhone 6" || modelName == "Simulator iPhone 6s" || modelName == "iPhone 6s" || modelName == "Simulator iPhone 7" || modelName == "iPhone 7" || modelName == "Simulator iPhone 8" || modelName == "iPhone 8" {
            self.upComingPlaylistView.frame = CGRect(x: self.upComingPlaylistView.frame.origin.x+20 , y: self.upComingPlaylistView.frame.origin.y + 450, width: self.upComingPlaylistView.frame.width-20, height: self.upComingPlaylistView.frame.height)
            self.playListLabel.font = UIFont.init(name: "OpenSans", size: 17)
        } else if modelName == "Simulator iPhone 8 Plus" || modelName == "iPhone 8 Plus" || modelName == "Simulator iPhone 7 Plus" || modelName == "iPhone 7 Plus" || modelName == "Simulator iPhone 6 Plus" || modelName == "iPhone 6 Plus" || modelName == "Simulator iPhone 6s Plus" || modelName == "iPhone 6s Plus" {
            self.upComingPlaylistView.frame = CGRect(x: self.upComingPlaylistView.frame.origin.x+30 , y: self.upComingPlaylistView.frame.origin.y + 530, width: self.upComingPlaylistView.frame.width, height: self.upComingPlaylistView.frame.height+10)
            self.playListLabel.font = UIFont.init(name: "OpenSans", size: 18)
        } else if modelName == "Simulator iPhone X" || modelName == "iPhone X" {
            self.upComingPlaylistView.frame = CGRect(x: self.upComingPlaylistView.frame.origin.x+20 , y: self.upComingPlaylistView.frame.origin.y + 555, width: self.upComingPlaylistView.frame.width-20, height: self.upComingPlaylistView.frame.height)
            self.playListLabel.font = UIFont.init(name: "OpenSans", size: 18)
        } else if modelName == "Simulator iPhone XR" || modelName == "iPhone XR" {
            self.upComingPlaylistView.frame = CGRect(x: self.upComingPlaylistView.frame.origin.x+30 , y: self.upComingPlaylistView.frame.origin.y + 630, width: self.upComingPlaylistView.frame.width, height: self.upComingPlaylistView.frame.height)
            self.playListLabel.font = UIFont.init(name: "OpenSans", size: 20)
        } else if modelName == "Simulator iPhone XS" || modelName == "iPhone XS" {
            self.upComingPlaylistView.frame = CGRect(x: self.upComingPlaylistView.frame.origin.x+20 , y: self.upComingPlaylistView.frame.origin.y + 555, width: self.upComingPlaylistView.frame.width-20, height: self.upComingPlaylistView.frame.height)
            self.playListLabel.font = UIFont.init(name: "OpenSans", size: 18)
        } else if modelName ==  "Simulator iPhone XS Max" || modelName ==  "iPhone XS Max" {
            self.upComingPlaylistView.frame = CGRect(x: self.upComingPlaylistView.frame.origin.x+30 , y: self.upComingPlaylistView.frame.origin.y + 640, width: self.upComingPlaylistView.frame.width, height: self.upComingPlaylistView.frame.height)
            self.playListLabel.font = UIFont.init(name: "OpenSans", size: 20)
            
        } else {
            self.upComingPlaylistView.frame = CGRect(x: self.upComingPlaylistView.frame.origin.x+50, y: self.upComingPlaylistView.frame.origin.y + 800, width: self.upComingPlaylistView.frame.width+315, height: self.upComingPlaylistView.frame.height + 50)
            self.playListLabel.font = UIFont.init(name: "OpenSans", size: 24)
            self.playButton.frame = CGRect(x: 0, y: 0, width: 70, height: 70)
        }
        
    }
    
    /**This function will help to close the rating view and along with song completion check.
- ## Logical SetUp :
     1) ratingview will be removed by clicking the close button.
     2) Songcompletioncheck will let you the status of that particular song.
     */
    func noThanks() {
        songCompletioncheck()
        rating.removeFromSuperview()
    }
    
    /** This function will help to confirm the rating is allocated for that song.

 - parameter ratingValue : It bring the rating value between 1 - 5
 - ## Logical SetUp :
     1) Ratingview will be removed once rating is given.
     2) Rating star will be updated depends upon user selection.
     3) Rating for the particular song  will be updated with new rating in the ratingArray.
     4) Rating array is saved in userdefaults for the key **DhwaniRatingArray** .
     5) Song completioncheck will help to setup next song Ui and audio for the player.
     */
    func rated(ratingValue:String!) {
        rating.removeFromSuperview()
        ratingvalue.text = ratingValue!
        ratingArray.replaceObject(at: index, with: ratingValue)
        Defaults().saveRatingDetails(value: ratingArray, key: "DhwaniRatingArray")
        songCompletioncheck()
    }
    
    /** This function is used to register CollectionViewCell.
- ## UI SetUp :
     1) Registering nib file is requied while collection view created in xib.
     */
    func registerCollectionView() {
        self.watchPlaylistCollectionView.register(UINib(nibName: NibNameConstants.MINDFULMOVEMENT_COLLECTIONVIEW_CELL, bundle: nil), forCellWithReuseIdentifier: NibNameConstants.MINDFULMOVEMENT_COLLECTIONVIEW_CELL)
    }
    
    /** This function will help to setup right gesture for eatch playlist
- ## UI SetUp :
     1) creating swipe right and swipe down with action.
     2) added Swipe right and Swipe left.
     
     */
    func setUpRightGesture () {
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizer.Direction.right
        self.watchPlaylistView.addGestureRecognizer(swipeRight)
        let swipeDowm = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeDowm.direction = UISwipeGestureRecognizer.Direction.down
        self.upComingPlaylistView.addGestureRecognizer(swipeDowm)
    }
    
    /**This function will help to respond to SwipeGesture type.
- parameter gesture : it holds gesture type (right, left, up , down )
 - ## UI SetUp :
 - ## case UISwipeGestureRecognizer.Direction.right:
     1) gesture will provide type of direction. if it is from right the neccessary action will be taken.
     2) watchplaylist will be transformed from left with transition speed of 4.0.
     3) upcoming playlist will be hidden. when it is direction from right.
     
- ## case UISwipeGestureRecognizer.Direction.down:
     1) gesture will provide type of direction. if it is from down the neccessary action will be taken.
     2) watchplaylist will be hidden and upcoming will be unhidden.
     */
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizer.Direction.right:
                let transition = CATransition()
                transition.type = CATransitionType.push
                transition.duration = 4.0
                transition.subtype = CATransitionSubtype.fromLeft
                self.view.addSubview(upComingPlaylistView)
                self.watchPlaylistView.frame = CGRect(x: self.watchPlaylistView.frame.origin.x, y: self.watchPlaylistView.frame.origin.y, width: self.watchPlaylistView.frame.width, height: self.watchPlaylistView.frame.height)
                self.watchPlaylistView.isHidden = true
                self.upComingPlaylistView.isHidden = false
                
            case UISwipeGestureRecognizer.Direction.down:
                self.upComingPlaylistView.frame = CGRect(x:self.upComingPlaylistView.frame.origin.x, y: self.upComingPlaylistView.frame.origin.y, width: self.upComingPlaylistView.frame.width, height: self.upComingPlaylistView.frame.height)
                self.view.addSubview(watchPlaylistView)
                self.upComingPlaylistView.isHidden = true
                self.watchPlaylistView.isHidden = false
                
            case UISwipeGestureRecognizer.Direction.left:
                print("Swiped left")
            case UISwipeGestureRecognizer.Direction.up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    
    /**This function will help to handle background player controls.
- ## UI SetUp :
     1)  Ui in the player screen will handled by backgroundplayer controls (play,pause,previous,forward) will be updated.
     */
    func backgroundModeAudioHandling() {
        UIApplication.shared.beginReceivingRemoteControlEvents()
        commandCenter.pauseCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the pause command
            self.buttonSetup()
            self.player?.pause()
            return .success
        }
        
        commandCenter.playCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            self.buttonSetup()
            self.player?.play()
            self.player?.playImmediately(atRate: 1.0)
            return .success
        }
        
        commandCenter.nextTrackCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            self.nextButtonAction((Any).self)
            self.buttonSetup()
            return .success
        }
        
        commandCenter.previousTrackCommand.addTarget { (event) -> MPRemoteCommandHandlerStatus in
            //Update your button here for the play command
            self.previousButtonAction((Any).self)
            self.buttonSetup()
            return .success
        }
    }
    
    /**This function will help to setup audioplayer.
- ## UI SetUp :
     1)  uiDataUpdation will handle to update audioplayer view with data(song url,authorname,songtitle,songbackgroundimage) from server.
     2) PrepareToPlay will handle assigning of remote URL to player and duration calculation of the song assign to audioduration label.
     3) updatePlayButtons will help to update audioplayer current status and actionablity of next and previous buttons according to song index.
     4) setUpCustomSlideBar will help to setup Audio player slidercontroller and create View design.
     */
    func audioSetup() {
        self.uiDataUpdation()
        self.preparetoplay()
        self.updatePlayButtons()
        setUpCustomSlideBar()
    }
    
    /**This function will help to assign song url and setting for auido player
- ## Logical Setup
      1) PrepareToPlay will handle assigning of remote URL to player and duration calculation of the song assign to audioduration label.
     2) AVAudioSession.sharedInstance().setCategory() will allow to play sound in mute mode of the device.
     
     */
    func preparetoplay() {
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [.mixWithOthers, .defaultToSpeaker,.allowAirPlay])
            
            try AVAudioSession.sharedInstance().setActive(true)
            
        } catch {
            print(error)
        }
        playerItem = AVPlayerItem(url: audioUrl!)
        player = AVPlayer(playerItem: playerItem)
        let audioAsset = AVURLAsset.init(url: audioUrl!, options: nil)
        let duration = audioAsset.duration
        self.endTime =  self.getFormatedTime(FromTime: Int(CMTimeGetSeconds(duration)))
        self.audioDuration.text = "00:00 " + " / " + self.endTime
    }
    
    /**This function will help to update audioplayer view with data(song url,authorname,songtitle,songbackgroundimage) from server
- ## UI SetUp :
     1) uiDataUpdation is updating the Ui from the data collected from Json response.
     
     */
    func uiDataUpdation() {
        let urldata = MindfulMovementArray.object(at:index) as! MindfulMovementModelData
        if urldata.author == "" {
            artistname.text = urldata.author
        } else {
            artistname.text = urldata.author
        }
        if urldata.songTitle == "" {
            songTitle.text = urldata.songTitle
            
        } else {
            songTitle.text = urldata.title
        }
        ratingvalue.text = ratingArray.object(at: index) as? String
        let downloadURL = NSURL(string:urldata.backgroundPic)!
        songBackgroundImage.af_setImage(withURL: downloadURL as URL )
        audioUrl = URL(string:urldata.AudioURL)
    }
    
    /**This function will help to update audioplayer current status and actionablity of next and previous buttons according to song index
- ## UI SetUp :
     1) updatePlayButtons is updating the UiButtons of previous/ next depends on the index value.
     2) if index is zero . previousButton will be hidden and next button will unhidden.
     3) if index is less than array count . previousButton will be unhidden and next button will hidden.
     4) if index is greater than array count . previousButton will be unhidden and next button will unhidden.
     */
    func updatePlayButtons() {
        if index == 0 {
            previousButton.isHidden = true
            nextButton.isHidden = false
        } else if index == MindfulMovementArray.count - 1 {
            previousButton.isHidden = false
            nextButton.isHidden = true
        } else {
            previousButton.isHidden = false
            nextButton.isHidden = false
        }
    }
    
    /**This Button action will trigger the function that will help to navigate to homescreen.
- parameter sender : UIButton.
- ## Logical SetUp :
     1) when Homebutton is triggered.Player will be paused and pops by back to previous screen.
     */
    @IBAction func homeButtonAction(_ sender: Any) {
        hideLoading()
        player?.pause()
        self.output.back()
    }
    
    /**This Button action will trigger the function that will help to download particular song.
- parameter sender : UIButton.
- ## Logical SetUp :
     1) currently download button is hidden to Ui visiblity and it is in coming soon mode.
     */
    @IBAction func downloadAction(_ sender: Any) {
        UIAlertController().showAlertMessage(vc: self, titleStr: "Alert", messageStr: "Coming Soon..!")
    }
    
    /**This function that will carry the response from API and will assign to model class and reload the view with new data.
- parameter result : It holds the dictionary value from API response.
- ## Logical SetUp :
     1) From the response **Result**. We will collect all value in model data format and will add each model data into a array.
     2) Array will be cleared before adding the new data to avoid the duplicate data.
     3) As we are not rating from the Json response need to add rating manual . if rating is nil/zero and will be saved in userdefaults.
     */
    func responseMindfulMovement(result:NSDictionary) {
        guard let tracklist = result[protocolKeyConstants.RESPONSE_TRACK_LIST] as? NSArray else { return }
        MindfulMovementArray.removeAllObjects()
        ratingArraysetup.removeAllObjects()
        for detail in tracklist {
            let details = detail as! NSDictionary
            let modalClass = MindfulMovementModelData().addMindfulMovementDetailswith(
                album: (details.object(forKey: protocolKeyConstants.RESPONSE_ALBUM)as? String ?? "")!,
                author: (details.object(forKey: protocolKeyConstants.RESPONSE_AUTHOR)as? String ?? "")!,
                backgroundPic: (details.object(forKey: protocolKeyConstants.RESPONSE_BACKGROUND_PIC)as? String ?? "")!,
                songTitle: (details.object(forKey: protocolKeyConstants.RESPONSE_DESCRIPTION)as? String ?? "")!,
                enabled: (details.object(forKey: protocolKeyConstants.RESPONSE_ENABLED)as? String ?? "")!,
                length: (details.object(forKey: protocolKeyConstants.RESPONSE_LENGTH)as? String ?? "")!,
                thumbnail: (details.object(forKey: protocolKeyConstants.RESPONSE_THUMBNAIL)as? String ?? "")!,
                title: (details.object(forKey: protocolKeyConstants.RESPONSE_TITLE)as? String ?? "")!,
                trackId: (details.object(forKey: protocolKeyConstants.RESPONSE_TRACK_ID)as? NSNumber ?? 0)!,
                trackType: (details.object(forKey: protocolKeyConstants.RESPONSE_TRACK_TYPE)as? String ?? "")!,
                AudioURL: (details.object(forKey: protocolKeyConstants.RESPONSE_URL)as? String ?? "")!,
                udid: (details.object(forKey: protocolKeyConstants.RESPONSE_UDID)as? NSNumber ?? 0)!)
            // rating: "0")
            
            MindfulMovementArray.add(modalClass)
            ratingArraysetup.add("0")
            
        }
        let data : Any? = Defaults().loadRatingDetailsFor(key: "DhwaniRatingArray")
        if data == nil {
            
        } else {
            ratingArray = data as! NSMutableArray
        }
        
        if ratingArraysetup.count != ratingArray.count {
            for _ in ratingArray.count..<ratingArraysetup.count{
                ratingArray.add ("0")
                
            }
            Defaults().saveRatingDetails(value: ratingArray, key: "DhwaniRatingArray")
        } else {
            
        }
        audioSetup()
        hideLoading()
    }
    
    /** This function get the status of rechability.
- ## Logical SetUp :
      1) ConnectivityManager.isReachable / network.reachability.whenReachable will indicates their is internet connection in device.
      2) ConnectivityManager.isUnreachable / network.reachability.whenUnreachable will indicates their is no internet connection in device.
     */
    func checkInternetConnetionStatus() {
        
        ConnectivityManager.isReachable { networkManagerInstance in
            UIAlertController().dismiss(animated: true, completion: nil)
            self.output.hitMindfulMovementApi()
        }
        
        ConnectivityManager.isUnreachable { networkManagerInstance in
            UIAlertController().internetsetting(vc: self)
        }
        
        network.reachability.whenReachable = { _ in
            UIAlertController().dismiss(animated: true, completion: nil)
        }
        
        network.reachability.whenUnreachable = { _ in
            UIAlertController().internetsetting(vc: self)
        }
        
    }
    
    /** This function will to trigger the action for playbutton and will change the play button to pause button. it will play with 1.0 bit rate to start quicker.
 - parameter sender : UIButton.
- ## Logical SetUp :
     1) when Playbutton is triggered.if song array count is zero pops up the alert telling no songs available.
     2) when song array have enough data . it will be played the song  with speed of 1.0. and alone updates the UI with pause button.
     */
    @IBAction func playButtonAction(_ sender: Any) {
        if MindfulMovementArray.count == 0 {
            UIAlertController().showAlertMessage(vc: self, titleStr: "Alert", messageStr: "No Songs Available.")
        } else {
            if playButton.currentImage!.isEqual(UIImage(named: ImageConstants.PAUSE_IMAGE) )  {
                player?.pause()
                playButtonEnable()
            } else {
                showLoading()
                player?.playImmediately(atRate: 1.0)
                startTimer()
                pauseButtonEnable()
            }
        }
    }
    
    /** This function will to trigger the action for previosButton and will change the current index.when their no songs in queue will disable the previous button for particular song.
 - parameter sender : UIButton.
- ## Logical SetUp :
     1) when previousbutton is triggered. index value will decremented by value 1.
     2) if player is playing mode. it will be pushed to previous song by playing . same when it is not playing. it will be paused mode for previous song.
     
     */
    @IBAction func previousButtonAction(_ sender: Any) {
        showLoading()
        self.audioSlider.value = 0.0
        index = index - 1
        
        self.startTime = "00: 00"
        setUpCustomSlideBar()
        if  self.player?.timeControlStatus == .playing
        {
            audioSetup()
            self.player?.play()
            player?.playImmediately(atRate: 1.0)
            startTimer()
            pauseButtonEnable()
            hideLoading()
            
        } else {
            audioSetup()
            self.player?.pause()
            playButtonEnable()
            hideLoading()
            
        }
        
    }
    
    /** This function will to trigger the action for nextButton and will change the current index and when their no songs in queue will disable the next button for particular song.
 - parameter sender : UIButton.
- ## Logical SetUp :
     1) when nextbutton is triggered. index value will incremented by value 1.
     2) when current index is greater than count of the array . it indicate that their is no previous song . previous button will be hidden.
     3)when current index is less than the count of the song array. if player is playing it will be pushed to next song by playing . same when it is not playing . it will be paused mode for next song.
     */
    @IBAction func nextButtonAction(_ sender: Any) {
        showLoading()
        index = index + 1
        if index > MindfulMovementArray.count - 1 {
            previousButton.isHidden = true
            hideLoading()
            UIAlertController().showAlertMessage(vc: self, titleStr: "Alert", messageStr: "You don't have a next song.")
        }
        else{
            self.audioSlider.value = 0.0
            if  self.player?.timeControlStatus == .playing
            {
                audioSetup()
                self.player?.play()
                player?.playImmediately(atRate: 1.0)
                startTimer()
                pauseButtonEnable()
            } else {
                if nextplayaction == true {
                    audioSetup()
                    self.player?.play()
                    player?.playImmediately(atRate: 1.0)
                    startTimer()
                    pauseButtonEnable()
                    nextplayaction = false
                } else {
                    audioSetup()
                    self.player?.pause()
                    playButtonEnable()}
                
                
            }
            
        }
    }
    
    /**This function will help to setup the background player controller. when app is locked.
- ## Logical SetUp :
     1) Button Setup will change the background player Ui according the current situation of the player here.
     */
    func buttonSetup() {
        if self.index == 0 {
            commandCenter.previousTrackCommand.isEnabled = false
            commandCenter.nextTrackCommand.isEnabled = true
            
            
        } else if self.index ==  MindfulMovementArray.count - 1 {
            commandCenter.previousTrackCommand.isEnabled = true
            commandCenter.nextTrackCommand.isEnabled = false
        } else {
            commandCenter.previousTrackCommand.isEnabled = true
            commandCenter.nextTrackCommand.isEnabled = true
        }
        
    }
    
    /**This function will help to start the timer with interval 1 sec.
- ## Logical SetUp :
     1) when timer is nil . it will allow timer to begin wit time interval of one second with action of update method.
     */
    func startTimer() {
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(MindfulMovementViewController.update(_:)), userInfo: nil,repeats: true)
            timer.fire()
            
        }
    }
    
    /**This function will help to stop the timer and make it nil.
- ## Logical SetUp :
     1) when timer is not nil . it will be maked as invalidate and also maked as nil.
     */
    func stopTimer() {
        if timer != nil {
            timer!.invalidate()
            timer = nil
        }
    }
    
    /**This method will trigger every single sec from the help of timer update. Accordingly change the song current time
- parameter timer : it holds time of the timer .
- ## Logical SetUp :
     1) If timecontrolstatus of player is playing.
     2) it will update the current time of the song.
     */
    @objc func update(_ timer: Timer){
        if  self.player?.timeControlStatus == .playing {
            hideLoading()
            self.playerItem = self.player?.currentItem
            let currentTime: Double = (self.playerItem?.currentTime().seconds)!
            let time = self.calculateTimeFromNSTimeInterval(currentTime)
            self.startTime  = "\(time.minute):\(time.second)"
            self.audioSlider.value = Float(currentTime)
            self.audioDuration.text = self.startTime + " / " + self.endTime
            self.songCompletioncheck()
        } else {
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    /** This method helps to convert date from int to string  with indivually minutes and seconds.
 - parameter timeDuration : Holds the time value in INT.
 - Returns : minute and seconds in String format
     
 - ## Logical SetUp :
     1) This method  brings the parameter of int value of time duration. will be converted in  string time format to play in UIlabel.
     */
    func calculateTimeFromNSTimeInterval(_ duration:TimeInterval) ->(minute:String, second:String){
        let minute_ = abs(Int((duration/60).truncatingRemainder(dividingBy: 60)))
        let second_ = abs(Int(duration.truncatingRemainder(dividingBy: 60)))
        let minute = minute_ > 9 ? "\(minute_)" : "0\(minute_)"
        let second = second_ > 9 ? "\(second_)" : "0\(second_)"
        return (minute,second)
    }
    
    /** This method helps to convert date from int to string .
 - parameter timeDuration : Holds the time value in INT.
 - Returns : date in String format
     
- ## Logical SetUp :
     1) This method  brings the parameter of int value of time duration. will be converted in  string time format to play in UIlabel.
     */
    func getFormatedTime(FromTime timeDuration:Int) -> String {
        let minutes = Int(timeDuration) / 60 % 60
        let seconds = Int(timeDuration) % 60
        let strDuration = String(format:"%02d:%02d", minutes, seconds)
        return strDuration
    }
    
    /**This function will enable the play button with play image
     */
    func playButtonEnable() {
        playButton.setImage(UIImage (named: ImageConstants.PLAY_IMAGE), for: .normal)
        hideLoading()
    }
    
    /**This function will enable the play button with pause image */
    func pauseButtonEnable() {
        playButton.setImage(UIImage (named: ImageConstants.PAUSE_IMAGE), for: .normal)
    }
    
}

/**This extension is to make cleancode for the collection view to the class */

extension MindfulMovementViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    /**This function will help to assign number of collection view cell need to create depending song array count */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return MindfulMovementArray.count
    }
    
    /**This function will help to assign data of collection view cell need to create depending song array */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: NibNameConstants.MINDFULMOVEMENT_COLLECTIONVIEW_CELL, for: indexPath) as! MindfulMovementCollectionViewCell
        let urldata = MindfulMovementArray[indexPath.row] as! MindfulMovementModelData
        cell.collectionImageview.af_setImage(withURL: URL(string: urldata.thumbnail)!)
        return cell
        
    }
    
     /**This function will help to assign size of collection view cell
- ## UI SetUp :
     1) getting the device model name  and adjusting collection view cell according to device height.
     */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let modelName = UIDevice.modelName
        if modelName == "Simulator iPhone 5s" || modelName == "iPhone 5s" || modelName == "Simulator iPhone SE" || modelName == "iPhone SE" || modelName == "Simulator iPhone 5c" || modelName == "iPhone 5c"{
            return CGSize(width: 50, height: 50)
        } else if modelName == "Simulator iPhone 6" || modelName == "iPhone 6" || modelName == "Simulator iPhone 6s" || modelName == "iPhone 6s" || modelName == "Simulator iPhone 7" || modelName == "iPhone 7" || modelName == "Simulator iPhone 8" || modelName == "iPhone 8" {
            return CGSize(width: 60, height: 60)
        } else if modelName == "Simulator iPhone 8 Plus" || modelName == "iPhone 8 Plus" || modelName == "Simulator iPhone 7 Plus" || modelName == "iPhone 7 Plus" || modelName == "Simulator iPhone 6 Plus" || modelName == "iPhone 6 Plus" || modelName == "Simulator iPhone 6s Plus" || modelName == "iPhone 6s Plus" {
            return CGSize(width: 70, height: 70)
        } else if modelName == "Simulator iPhone X" || modelName == "iPhone X" {
            return CGSize(width: 65, height: 65)
        } else if modelName == "Simulator iPhone XR" || modelName == "iPhone XR" {
            return CGSize(width: 68, height: 68)
        } else if modelName == "Simulator iPhone XS" || modelName == "iPhone XS" {
            return CGSize(width: 65, height: 65)
        } else if modelName ==  "Simulator iPhone XS Max" || modelName ==  "iPhone XS Max" {
            return CGSize(width: 65, height: 65)
        } else {
            return CGSize(width: 80, height: 80)
        }
    }
    
    /**This function will help to navigate the selection of particular collection view cell need to play particular song
-## Logical setup:
     1) From the selection indexpath is passed to index value. will be defaultly assigned start time and audioslider value to zero for every new index pressed.
     2) By selecting new index current song need to paused  if player.rating is not equal to zero.
     3) player.rating equal to zero . will be setup new index of the remote url array . pause button ui will be appear.
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        showLoading()
        self.index = indexPath.row
        startTime = "00:00"
        audioSlider.value = 0.0
        if  !(self.player?.rate != 0 && self.player?.error == nil)
        {
            audioSetup()
            self.player?.pause()
            playButtonEnable()
            hideLoading()
        } else {
            audioSetup()
            player?.playImmediately(atRate: 1.0)
            pauseButtonEnable()
        }
    }
}
