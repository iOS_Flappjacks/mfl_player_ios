//
//  MindfulMovementCollectionViewCell.swift
//  MFL Player
//
//  Created by Omkar on 2/15/19.
//

import UIKit

class MindfulMovementCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var collectionImageview : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                collectionImageview.layer.borderWidth = 3.0
                collectionImageview.layer.borderColor = UIColor.white.cgColor
            } else {
                collectionImageview.layer.borderWidth = 3.0
                collectionImageview.layer.borderColor = UIColor.clear.cgColor            }
        }
    }
}
