import Foundation

protocol MindfulMovementInteractorOutput: class {
    /**
     This method return the response to Update UI
     - Parameter result : holds NSDictionary. */
    func responseMindfulMovement(result:NSDictionary)
}
