//
//  MindfulMovementInteractor.swift
//  MFL Player
//
//  Created by Omkar on 2/8/19.
//  Copyright 2019 ___ORGANIZATIONNAME___. All rights reserved.
//
// -----------------------------------------------------------------



import Foundation

class MindfulMovementInteractor: MindfulMovementInteractorInput {
    
    weak var output: MindfulMovementInteractorOutput!
    var net = NetService()
   /** This method will help to hit the MindfulMovementAPI and return the response. */
   func hitMindfulMovementApi() {
    net.hitGetAPIWith(url: UrlConstants.MINDFUL_MOVEMENT_API) { (value) in
        let result = value as NSDictionary?
        self.output.responseMindfulMovement(result: result  ?? [:])
    }
    }
}
