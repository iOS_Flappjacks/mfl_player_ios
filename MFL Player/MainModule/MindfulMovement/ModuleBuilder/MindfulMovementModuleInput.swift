import Foundation

protocol MindfulMovementModuleInput: class {
    
    func configureModule()
}
