import UIKit

class MindfulMovementRouter:MindfulMovementRouterInput {
    
    weak var viewController: MindfulMovementViewController!
    
    func back() {
        viewController.navigationController?.popViewController(animated: true)
    }
    
}
