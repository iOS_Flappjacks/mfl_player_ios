import UIKit

class MindfulEatingViewController: UIViewController, MindfulEatingViewInput {

    var output: MindfulEatingViewOutput!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
     /**This Button action will trigger the function that will help to navigate to homescreen. */
    @IBAction func homeButtonAction(_ sender: Any) {
        self.output.back()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
