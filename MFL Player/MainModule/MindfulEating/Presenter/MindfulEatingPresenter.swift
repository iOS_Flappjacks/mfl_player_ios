import Foundation

class MindfulEatingPresenter: MindfulEatingModuleInput, MindfulEatingViewOutput, MindfulEatingInteractorOutput {
    
    weak var view: MindfulEatingViewInput!
    var interactor: MindfulEatingInteractorInput!
    var router: MindfulEatingRouterInput!
    
    // MARK: - MindfulEatingViewOutput
    
    func back() {
        router.back()
    }
    
    // MARK: - MindfulEatingModuleInput
    
    func configureModule() {
        
    }
    
    // MARK: - MindfulEatingInteractorOutput
    
}
